# GraceDB client software
Client software for the <b>Gra</b>vitational-wave <b>C</b>andidate <b>E</b>vent <b>D</b>ata<b>b</b>ase, a web service that organizes candidate events from gravitational wave searches and provides an environment to record information about follow-ups.
Further documentation is available at https://wiki.ligo.org/DASWG/GraceDB and https://gracedb.ligo.org/documentation/.

## Workflow
* Bug reports and feature requests: submit at https://bugs.ligo.org/redmine/projects/gracedb.
* Patches: fork this repository, make your changes, and submit a merge request. For record-keeping purposes, please submit a description of the patch on https://bugs.ligo.org/redmine/projects/gracedb, including a link to the merge request.
